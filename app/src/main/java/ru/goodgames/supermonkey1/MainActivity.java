package ru.goodgames.supermonkey1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v4.util.Pair;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.facebook.applinks.AppLinkData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import ru.goodgames.supermonkey1.activity.WebViewActivity;
import ru.goodgames.supermonkey1.common.Constants;

import static ru.goodgames.supermonkey1.common.Constants.AF_DEV_KEY;

public class MainActivity extends MyActivity {

    private AppLinkData.CompletionHandler handler = new AppLinkData.CompletionHandler() {
        @Override
        public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
            System.out.println("----------------------AppLinkData--------------------");
            if (appLinkData != null) {
                Uri link = appLinkData.getTargetUri();
                if (link != null) {
                    List<Pair<String, String>> params = getParamsFromUri(link);
                    goToWebViewActivity(Constants.TRACKER_URL, params);
                }
            }
            goToWebView();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setStatusBarTransparent();
        setContentView(R.layout.main_activity);

        AppsFlyerLib.getInstance().startTracking(getApplication(), "application");
        AppsFlyerLib.getInstance().init(AF_DEV_KEY, new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> map) {
                if (map.containsKey(Constants.DEFERED_LINK_KEY) && map.containsKey(Constants.IS_FIRST_LAUNCH_KEY) && map.get(Constants.IS_FIRST_LAUNCH_KEY).equals("true")) {
                    String value = map.get(Constants.DEFERED_LINK_KEY);
                    Uri uri = Uri.parse(value);
                    List<Pair<String, String>> params = getParamsFromUri(uri);
                    goToWebViewActivity(Constants.TRACKER_URL, params);
                } else if (map.containsKey(Constants.IS_FIRST_LAUNCH_KEY) && !Boolean.parseBoolean(map.get(Constants.IS_FIRST_LAUNCH_KEY))) {
                    //When user press skip, start Main Activity
                    goToWebView();
                } else {
                    AppLinkData.fetchDeferredAppLinkData(getApplicationContext(), handler);
                }
            }

            @Override
            public void onInstallConversionFailure(String s) {

            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {
                System.out.println("------------------AppsFlyer---------------------");
                if (map == null || map.isEmpty()) {
                    return;
                }
                String value = map.get(Constants.APPSFLYER_KEY_LINK);
                if (value == null) {
                    goToWebViewActivity(Constants.DEFAULT_URL);
                }
                System.out.println(value);
                Uri uri = Uri.parse(value);
                List<Pair<String, String>> params = getParamsFromUri(uri);
                goToWebViewActivity(Constants.TRACKER_URL, params);
            }

            @Override
            public void onAttributionFailure(String s) {

            }
        }, getApplicationContext());
        AppsFlyerLib.getInstance().sendDeepLinkData(this);

    }

    private void goToWebView() {
        String savedId = getId();
        System.out.println("MAIN ACtivity");
        if (savedId != null) {
            goToWebViewActivity(Constants.TRACKER_URL);
        } else {
            goToWebViewActivity(Constants.DEFAULT_URL);
        }
    }

    private void goToWebViewActivity(String url) {
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_EXTRA, url);
        intent.putExtras(bundle);
        startActivity(intent);

        finish();
    }

    private void goToWebViewActivity(String urlKey, List<Pair<String, String>> params) {
        putParams(params);
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_EXTRA, urlKey);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void putParams(List<Pair<String, String>> params) {
        SharedPreferences sharedPref = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        String key = sharedPref.getString(Constants.ID_KEY, null);
        if (key == null) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(Constants.ID_KEY, UUID.randomUUID().toString());
            if (params != null) {
                for (Pair<String, String> pair : params) {
                    editor.putString(pair.first, pair.second);
                }
            }
            editor.commit();
        }
    }

    private void setStatusBarTransparent() {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private List<Pair<String, String>> getParamsFromUri(Uri uri) {
        String s1 = uri.getQueryParameter(Constants.SUB_1_QUERY_PARAM);
        String s2 = uri.getQueryParameter(Constants.SUB_2_QUERY_PARAM);
        String s3 = uri.getQueryParameter(Constants.SUB_3_QUERY_PARAM);
        String s4 = uri.getQueryParameter(Constants.SUB_4_QUERY_PARAM);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
        List<Pair<String, String>> params = new ArrayList<>();
        if (s1 != null) {
            params.add(new Pair<String, String>(Constants.SUB_1_QUERY_PARAM, s1));
        }
        if (s2 != null) {
            params.add(new Pair<String, String>(Constants.SUB_2_QUERY_PARAM, s2));
        }
        if (s3 != null) {
            params.add(new Pair<String, String>(Constants.SUB_3_QUERY_PARAM, s3));
        }
        if (s4 != null) {
            params.add(new Pair<String, String>(Constants.SUB_4_QUERY_PARAM, s4));
        }

        return params;
    }
}
