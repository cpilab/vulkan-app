package ru.goodgames.supermonkey1.common;

public final class Constants {
    public static final String IS_FIRST_LAUNCH_KEY = "is_first_launch";
    public static final String DEFERED_LINK_KEY = "af_dp";
    public static final String USERS_KEY = "users";
    public static String SUB_1_QUERY_PARAM = "sub_id_1";
    public static String SUB_2_QUERY_PARAM = "sub_id_2";
    public static String SUB_3_QUERY_PARAM = "sub_id_3";
    public static String SUB_4_QUERY_PARAM = "sub_id_4";
    public static String FACEBOOK_QUERY_PARAM = "url";
    public static String KEY_EXTRA = "firebaseKey";
    public static String APPSFLYER_KEY_LINK = "link";
    public static String FACEBOOK_KEY_LINK = "link";
    public static String DEFAULT_URL = "defaultURL";
    public static String TRACKER_URL = "trackerURL";
    public static final String AF_DEV_KEY = "vourgpamQDRBFhexAjqmHB";
    public static final String ID_KEY = "id";
    public static final String COOKIE_KEY = "cookie";
    public static final String APP_PREFERENCES = "settings";
}
