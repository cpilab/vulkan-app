package ru.goodgames.supermonkey1;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import ru.goodgames.supermonkey1.common.Constants;

public abstract class MyActivity extends Activity{
    protected String getId() {
        SharedPreferences sharedPref = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.ID_KEY, null);
    }
}
