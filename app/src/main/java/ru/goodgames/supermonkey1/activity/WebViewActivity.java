package ru.goodgames.supermonkey1.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.telephony.TelephonyManager;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import ru.goodgames.supermonkey1.MyActivity;
import ru.goodgames.supermonkey1.R;
import ru.goodgames.supermonkey1.common.Constants;

public class WebViewActivity extends MyActivity {

    private static final String URL_KEY = "url";

    private WebView mWebView;
    private String url = "https://developer.android.com";
    private DatabaseReference myRef;
    private String urlKey = "defaultURL";
    private String savedURL = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String key = null;
        TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (savedInstanceState != null) {
            key = savedInstanceState.getString(Constants.KEY_EXTRA);
        } else if (this.getIntent() != null) {
            key = this.getIntent().getStringExtra(Constants.KEY_EXTRA);
        }
        if (key != null) {
            urlKey = key;
        }
        setContentView(R.layout.web_view_activity);
        mWebView = findViewById(R.id.WebView2);
        mWebView.getSettings().setJavaScriptEnabled(true);
        myRef = FirebaseDatabase.getInstance().getReference();
        CookieSyncManager.createInstance(getBaseContext());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.child(urlKey).getValue(String.class);

                if ((value == null || url.contains(value))) {
                    return;
                }
                if (!url.equals(savedURL) && savedURL != null) {
                    url = savedURL;
                } else {
                    url = value;
                }
                List<Pair<String, String>> params = getParams();
                if (Constants.TRACKER_URL.equals(urlKey) && !params.isEmpty()) {
                    url += "?";
                    for (Pair<String, String> pair : params) {
                        if (url.contains(pair.first + "=" + pair.second)){
                            continue;
                        }
                        url += pair.first + "=" + pair.second;
                        if (params.indexOf(pair) != params.size() - 1) {
                            url += "&";
                        }
                    }
                }
                System.out.println(url);
                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        CookieSyncManager.getInstance().sync();
                        findViewById(R.id.WebView2).setVisibility(View.VISIBLE);
                    }
                });
                mWebView.loadUrl(url);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
        String idUser = getId();
        if (idUser != null) {
            Map<String, Object> map = Collections.singletonMap(idUser,
                    (Object) (Build.DEVICE + " " + Build.MODEL + " " + mTelephonyManager.getNetworkOperator()));
            myRef.child(Constants.USERS_KEY).updateChildren(map);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        bundle.putString(Constants.KEY_EXTRA, urlKey);
        bundle.putString(URL_KEY, mWebView.getUrl());
        super.onSaveInstanceState(bundle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            String url = savedInstanceState.getString(URL_KEY);
            if (url != null){
                this.savedURL = url;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CookieSyncManager.getInstance().startSync();
    }

    @Override
    protected void onPause(){
        super.onPause();
        CookieSyncManager.getInstance().stopSync();
    }

    private List<Pair<String, String>> getParams() {
        SharedPreferences sharedPref = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        List<Pair<String, String>> result = new ArrayList<>();
        String s1 = sharedPref.getString(Constants.SUB_1_QUERY_PARAM, null);
        String s2 = sharedPref.getString(Constants.SUB_2_QUERY_PARAM, null);
        String s3 = sharedPref.getString(Constants.SUB_3_QUERY_PARAM, null);
        String s4 = sharedPref.getString(Constants.SUB_4_QUERY_PARAM, null);
        if (s1 != null) {
            result.add(new Pair<String, String>(Constants.SUB_1_QUERY_PARAM, s1));
        }
        if (s2 != null) {
            result.add(new Pair<String, String>(Constants.SUB_2_QUERY_PARAM, s2));
        }
        if (s3 != null) {
            result.add(new Pair<String, String>(Constants.SUB_3_QUERY_PARAM, s3));
        }
        if (s4 != null) {
            result.add(new Pair<String, String>(Constants.SUB_4_QUERY_PARAM, s4));
        }
        return result;
    }
}
